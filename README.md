# Handemade Quake JavaScript

[Philip Buuck](https://twitter.com/philipbuuck) is doing a Handemade Quake series.
I am following along the C centric videos and implementing it in JavaScript.

Please consider donating to [Philip's Patreon](http://bit.ly/1UsXwF5)!

Talk about Handemade Quake [on Reddit](http://bit.ly/1WhdIcW).

## JavaScript Notes

Using Node.js, Chrome, and Electron

## Running

Install [node.js](https://nodejs.org/en/)

Install dependencies in the root of the project:
```sh
$ npm install
```

Run the application:
```sh
$ npm start
```
